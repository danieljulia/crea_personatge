/**
funcions generals
*/

function getHashValue(key) {
  var matches = location.hash.match(new RegExp(key+'=([^&]*)'));
  return matches ? matches[1] : null;
}


var audios=[];
var locutions_played=[];

function playAudio(name,has_lang,loop){
  if(!config.sound) return;
  if(has_lang!=undefined){
    if(locutions_played.indexOf(name)!=-1) return;
    locutions_played.push(name);
    name="resources/locutions/"+name+"_"+lang;

  }else{
    name='resources/sounds/'+name;
  }

  var audio = new Audio(name+'.mp3');
  if(loop) audio.loop=true;
  audio.play();
  audios.push(audio);
}

function audioStopAll(){
  for(var i=0;i<audios.length;i++){
    audios[i].pause();
  }
}

function setText(sel,label){
  $(sel).text(getText(label));
}

function getText(label){
  var txt=labels[label+"_"+lang];
  if(txt==undefined) return "undefined: "+label;
  return txt;
}


function animSprite(sel,im1,im2,times){

  $(sel).attr('src',im2);
  var c=0;
  doit();
  function doit(){
      console.log("**animant.. ",c,times);
    if(c%2==0){
      $(sel).attr('src',im1);
      console.log(im1);
    }else{
      $(sel).attr('src',im2);
      console.log(im2);
    }
    setTimeout(function(){
      c++;
      if(c<times) doit();
    },100);
  }
}



function over_transparent(sel,cb){
  $(sel).on('mouseover click',function(){
      playAudio("click");
    $(this).css({opacity:1});
    cb();
  });
  $(sel).on('mouseout',function(){
    $(this).css({opacity:0});
  });
}

function anim_from_top(sel){
  var top=$(sel).css('top');
  $(sel).css('top','-500px');
  $(sel).velocity({
    top:top
  },{
    duration:1000
  //  complete:cb()
  });
  $(sel).removeClass('hidden');
}

function anim_to_top(sel){
  $(sel).velocity({
    top:'-500px'
  },{
    duration:1000
  //  complete:cb()
  });

}

function fadeIn(sel,cb){
    $(sel).velocity("stop", true);
  var par={duration:500};
  if(cb!=undefined) par.complete=cb();

  $(sel).velocity({
    opacity:1
  },par);
}

function fadeOut(sel,cb){
      $(sel).velocity("stop", true);
  var par={duration:500};
  if(cb!=undefined) par.complete=cb();

  $(sel).velocity({
    opacity:0
  },par);
}


function random(n){
  return Math.floor(Math.random()*n);
}
