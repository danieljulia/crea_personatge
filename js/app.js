'use strict';

var lang;
var mouseX,mouseY;

var app;



$(document).ready(function(){
    app=new App(lang);

    /*
  $(document).on('mousemove',function(evt){
    mouseX = evt.pageX - $('.main-content').offset().left;
    mouseY = evt.pageY - $('.main-content').offset().top;

  });*/
});



function App(){
  var hashlang=getHashValue("lang");
  if(!hashlang){
    if(config.lang!=undefined) lang=config.lang;
    else lang="ca";
  }
  else lang=hashlang;


  if(lang=="es"){

    $('#logo').attr('src','images/logo-recurs-educatiu-es.png');


  }


if(!config.debug){
  $('#debug').hide();
}
   this.current=config.start;
   this.pers={
     personatge:'',
   };
   this.eventsBound=[];
   this.init();

}

App.prototype.init=function(){

  this.paint=new DragPaint('.paleta');

    //carregar texts
    $("[data-label]").each(function() {
      var label = $(this).data("label");



      if (labels[label + "_" + lang] != undefined) {
        $(this).html(labels[label + "_" + lang]);
      } else {
        console.log("label no trobat: "+label + "_" + lang);
      //  $(this).html(label + "_" + lang);
      }
    });


  this.interface();
  this.updateFase();
}

App.prototype.go=function(step){
    $('.fase'+this.current+' .sprite').velocity("stop",true);
  playAudio("plem");
    this.current+=step;
    if(this.current<0) this.current=0;
    //todo limit final?
    this.updateFase();
}


App.prototype.back=function(){
      $('.fase'+this.current+' .sprite').velocity("stop",true);
  var that=this;
  if(this.current==5){
    this.current=0;
    this.updateFase();
    return;
  }
  that.go(-1);

  that.updateFase();

}

App.prototype.interface=function(){


  var that=this;


}

App.prototype.updateFase=function(){

  $('.fase').hide();
  $('.fase'+this.current).show();
  if(this.current==3) $('.fase'+this.current).css('display','flex');
  this.initStatus();
}

App.prototype.setBehaviours=function(sel){

if(sel==undefined) sel="";
//else sel=".fase"+this.current;

$(sel+' .pop').off('mouseenter').on('mouseenter',function(ev){



  if($(this).data('selected')) return;

  //if($(this).hasClass("static")) $(this).data('selected',true);
  playAudio('plop');
  var r=Math.floor(Math.random()*10-5);
//  var t=$(this).css('transform');
  //console.log("transform",t);

  $(this).velocity("stop", true);
  $(this).velocity(
    {
        scale:[1.1,1]
    },
    {
    duration: 600,
        easing:[200,5]
  }
  );
  ev.stopPropagation();
});

$(sel+' .pop').off('mouseleave').on('mouseleave',function(){
  //  if($(this).hasClass("static")) $(this).data('selected',false);
if($(this).data('selected')) return;

  $(this).velocity("stop", true);

  if($(this).hasClass('anim-balanceig')){

    //var rot=$(this).css('transform');


    var r=Math.floor(Math.random()*10-5);
    r=0;

    $(this).velocity(
        {
          rotateZ:[(r-5)+"deg", (r+5)+"deg)"],
          scale:[1,1.2]
        },
        {
        duration: 1500,
        loop: 1000 // Loop
      }
    );
  }else{

    $(this).velocity(
      {
        scale:[1,1.2]
      },
      {
      duration: 100
    }
    );
  }

});



$(sel+' .anim-balanceig').each(function(){

  var r=Math.floor(Math.random()*30-15);


  $(this).velocity(
    {
        rotateZ:[(r-5)+"deg", (r+5)+"deg)"],
    },
    {
    duration: 2000,
    loop: 1000 // Loop
  }
);
});

}

App.prototype.initStatus=function(){
  var that=this;

  this.setBehaviours();

  switch (this.current) {
    case 0:

      $('.tornar').hide();
      if(!monster_played)
        monster();
      break;

    case 1:
        $('.tornar').show();
        playAudio('plop');

        $('.fase1 .quadre').velocity("stop", true);

        $('.fase1 .quadre').velocity(
          {
              scale:[1,0.5]
          },
          {
          duration: 1200,
          easing:[250,5]
        });

/*
        $('.quadre').velocity(
          {
            transform: ["rotateZ(-5deg)", "rotateZ(5deg)"]
          },
          {
          duration: 500,
              loop: 10 // Loop
            }
          );*/
          //monster();
          break;

      case 2:

          $('.fase2 .personatge').data('selected',false);
          $('.fase2 .quadre').velocity("stop", true);

/*
          $('.fase2 .quadre').velocity(
            {
                scale:[1,0.5]
            },
            {
            duration: 1200,
            easing:[250,5],
            complete:function(){

                that.setBehaviours();

            }
          });
          */
          that.setBehaviours();

      $('.fase.personatges .quadre').off("click").on('click',function(ev){

          $(this).data('selected',true);

          var img=$(this).find('img').attr('src');
          var perso=$(this).data('perso');

          //popup

          var left=$(this).css('left');
          var top=$(this).css('top');

          $(this).velocity("stop");

          $(this).velocity({
            /*scale:0.5,*/
            rotateZ:0,
            left:'350px',
            top:'60px',

          },
        {
          duration:200,
          easing: "spring",
          complete:function(){

            //$('#popup .content').html($('.llimona'));
            var html='<div class="personatge"><img width="500"></div>';
            html+="<nav class='barra'><img id='cancel' class='button pop' src='images/botons/boto_creu_fons.png'><h2 id='msg-bottom' data-text='vols'>"+
            getText("vols")+"</h2><img id='ok' class='button pop' src='images/botons/boto_ok_fons.png'>";


            popup.open(html);

            $('#popup .personatge img').attr('src',img);
            $('#popup #ok').off("click").on('click',function(ev){
                that.pers.personatge=perso;
                that.como(function(){
                  that.go(1);

                });
                playAudio("SoundEnter");
                //obrir l'altre popup
                ev.preventDefault();
            });
            $('#popup #cancel').off("click").on('click',function(ev){
              playAudio("SoundClose");
                popup.close();
                ev.preventDefault();
                  $('.fase2 .personatge').data('selected',false);





                  $('.fase2 .personatge').each(function(){

                    $(this).velocity("stop", true);
                    $(this).data('selected',false);

                  });

                  that.setBehaviours();




            });
            that.setBehaviours('#popup');
            $(this).css({left:left,top:top});

          }
        });

          playAudio("pleep");
      });

      break;

      case 3: //pintar



        this.nom();

          if(that.pers.personatge==""){
            that.pers.personatge="monstre";
          }
          this.paint.setPersonatge(that.pers.personatge);
          this.paint.setPart('ulls');

          if(this.eventsBound[3]==undefined){
              $('.fase3 .listo').off('click').on('click',function(ev){

                var msg=that.paint.validate();
                if(msg==""){
                  that.go(1);
                  return;
                }
                that.error(getText("avis_"+msg));
                if(msg=="ulls" || msg=="nas" || msg=="boca" || msg=="pecu"){
                    that.paint.setPart(msg);
                }


                if(msg=="pecu_text"){
                  that.popuppecu();
                }

                if(msg=="mon"){
                  that.mons();

                }


                ev.stopPropagation();
              });
          }


        break;

    case 4: //nom

    var sils=[
      ['-','fu','po','res','mi','per'],
        ['-','gu','ka','ta','le','bo'],
          ['-','ni','bont','rin','tzal','tot']
        ];
        var sil_pos=[0,0,0];


        updateNom();
        this.pers.nom="";


          $('.silabes .sil').off('click').on('click',function(){

            playAudio("SoundNameChoose");
            var i=$(this).data('i');

            sil_pos[i]++;
            if(sil_pos[i]==sils[i].length) sil_pos[i]=0;
            updateNom();
          });

          $('.fase4 .next').off('click').on('click',function(){
            if(  that.paint.misc.nom==""){

              playAudio("SoundError");
              return;
            }
            that.go(1);
          });

        //  this.eventsBound[4]=true;


        function updateNom(){
            that.paint.misc.nom="";
            $('.silabes .sil').each(function(i,item){
              var sil=sils[i][sil_pos[i]];
              $(this).text(sil);
              if(sil!="-")     that.paint.misc.nom+=sil;
            });
            $('.fase4 .nom').text(   that.paint.misc.nom);

        }


      break;

      case 5:
      var that=this;
        if(fullscreen){
          document.exitFullscreen();
          setTimeout(function(){
            that.paint.paint();
            that.doCanvas();
          },2500);
        }else{
          that.paint.paint();
          that.doCanvas();
        }
        playAudio("SoundPhoto");




    default:

  }
}

App.prototype.doCanvas=function(){
  //afegir sprites
  var that=this;


  if(config.canvas){// && !fullscreen){

    //html2canvas(document.querySelector(".fase5")).then(function(canvas) {
    html2canvas(document.querySelector(".fase5"),{
      onrendered:function(canvas){
        document.querySelector(".fase5").appendChild(canvas);
        $('.fase5 h2').remove();
          $('.fase5 .perso').remove();
        $('canvas').attr('id','myCanvas');
            $( "#myCanvas" ).wrap( "<a download='"+that.paint.misc.nom+".jpg' href='' id='download'></a>" );
        $('.fase5').on('click',function(){
            download();
        });
      }
    });

  //).then(function(canvas) {


  //  });
  }

  function download(){
    var download = document.getElementById("download");
    var image = document.getElementById("myCanvas").toDataURL("image/png")
                    .replace("image/png", "image/octet-stream");
        download.setAttribute("href", image);
        //download.setAttribute("download","archive.png");
  }

}


App.prototype.credits=function(){
  var html='<div id="ok" class="fase1 credits"><div  class="pop static button anim-balanceig quadre"><div class="content">';

  html+=getText("credits_msg");

  html+="</div></div></div>";

  popup.open(html);
$('#popup').addClass("credits");

  $('#popup #ok').off("click").on('click',function(ev){
      popup.close();
      $('#popup').removeClass("credits");
      ev.preventDefault();
  });

}
App.prototype.error=function(msg){
  var that=this;
  $('.error').text(msg);
  $('.error').fadeIn();

  playAudio("SoundError");
  this.error_time=new Date().getTime();
  setTimeout(function(){
    var t=new Date().getTime();


    if((t-that.error_time)>=4800){
      $('.error').fadeOut();
      this.error_time=-1;
    }
  },5100);
}


App.prototype.error_lang=function(label){
  var txt=labels[label+"_"+lang];
  if(txt==undefined) txt=":"+label+":";
  this.error(txt);

}

App.prototype.mons=function(ev){
  if(ev!=undefined)
      ev.stopPropagation();

  this.error(getText("mon"));

  var that=this;
  //popup per triar peculiaritat
  var html="<h2 data-text='comets'>"+getText("on")+"</h2><div class='on'>";
  var mons_text=eval("mons_"+lang);

  for(var i=0;i<mons_img.length;i++){
    html+="<div data-id='"+i+"' class='button anim-balanceig mon mon"+i+" pop'><img src='images/fons/"+mons_img[i]+"_q.png'><p>"+mons_text[i]+"</p></div>";
  }
  html+="</div>";
  popup.open(html,function(i){
    that.paint.misc.mon=i;
    $('.carta .mon').attr('src','images/fons/'+mons_img[i]+'_q.png');

  },false);

}

App.prototype.como=function(cb){
  var that=this;

  var html="<h2 data-text='comets'>"+getText("com")+"</h2><div class='buttons como'>";
  var como=eval("como_"+lang);
  for(var i=0;i<12;i++){
    html+="<div data-id='"+i+"' class='button round pop col-3'>"+como[i]+"</div>";
  }
  html+="</div>";
  popup.open(html,function(i){

    that.paint.misc.como=i;

    that.go(1);

  });

}



App.prototype.nom=function(){
  var nom=this.paint.getComoText();
  if(this.paint.misc.pecu_text!=""){
    nom+=", "+this.paint.misc.pecu_text;
  }
  $('h4.como').text(nom);

}

App.prototype.popuppecu=function(cb){
  var that=this;
  //popup per triar peculiaritat
  var html="<h2 data-text='comets'>"+getText("teu")+"</h2><div class='buttons pecu'>";
  var pecu=eval("pecu_"+lang);
  for(var i=0;i<pecu.length;i++){
    html+="<div data-id='"+i+"' class='button round pop col-3'>"+pecu[i]+"</div>";
  }
  html+="</div>";
  popup.open(html,function(i){
    var pecu=eval("pecu_"+lang);
    that.paint.misc.pecu_text=pecu[i];
    that.nom();
  });
}






function Popup(){

}

Popup.prototype.open=function(html,cb,colors){
  var that=this;
  if(colors==undefined) colors=true;
  $('#popup').removeClass();
  $('#popup').addClass("bfase"+app.current);
  if($('#popup').length==0){
    $('.main-content').append("<div id='popup'><div class='content'></div></div>");
    //$('#popup').html("aquest és el contingut del popup");
  }



  $('#popup .content').html(html);

  if(colors){

    var codis=['C46867','FAC931','A86E6F','7D7CB0','7BBE58',
    '5B74BC','9C7C4F','F08139','BC4CCB','A1AF7C','CD4C53','70ABDA'];

    $('#popup .buttons .button').each(function(i){
      $(this).css({'background-color':'#'+codis[i]});
    });
  }

  if(cb!=undefined){
    $('#popup .button').on('click',function(){
      var id=$(this).data('id');
      cb(id);
        that.close();
        $('.error').fadeOut();
    })
  }

  app.setBehaviours('#popup');
  $(' #popup').show();
}



Popup.prototype.close=function(){
  $('#popup').hide();
  $('#popup .content').html('');
}

var popup=new Popup();


var monster_c=1;
var monster_played=false;


function monster(){
  if(app==undefined){
    console.log("esperant...");
    setTimeout(function(){
        if(app.current<=1){
          monster();
      }
    },1000);
    return;
  }
  if(app.current<=1){
    monster_played=true;
    $('.fase'+app.current+" .sprite").velocity("stop",true);

    $('.fase'+app.current+" .sprite").remove();
    $('.fase'+app.current).append('<div class="sprite"><img></div>');
  }else{
    return;
  }
  //$('.sprite img').velocity("stop");


  //if(monster_playing) return;
//  monster_playing=true;
  //agafa un monstre aleatori i el mostra entrant i sortint
  $('.fase'+app.current+" .sprite img").attr('src','images/perso_'+monster_c+'.png');
  monster_c++;
  if(monster_c==6) monster_c=1;

  var pos=[


    {
      ini:{top:'-1000px',left:'500px',transform:'scale(1.2) rotate(180deg)'},
      end:{left:'400px',top:'-300px',}
    },


    {
      ini:{top:'1616px',left:'420px',transform:'scale(1.2) rotate(0deg)'},
      end:{left:'420px',top:'420px',}
    },

    { //a baix esquerra
      ini:{top:'600px',left:'-350px',transform:'scale(1.2) rotate('+(180-45-90)+'deg)'},
      end:{left:'20px',top:'280px',}
    },


    { // a baix dreta
      ini:{top:'600px',left:'1350px',transform:'scale(1.2) rotate('+(-45)+'deg)'},
      end:{left:'850px',top:'280px',}
    },

    { //a dalt esquerra
      ini:{top:'-600px',left:'-350px',transform:'scale(1.2) rotate('+(-45-180)+'deg)'},
      end:{left:'-20px',top:'-200px',}
    },

    { // a dalt dreta
      ini:{top:'-800px',left:'1150px',transform:'scale(1.2) rotate('+(-45-90)+'deg)'},
      end:{left:'850px',top:'-200px',}
    }
  ];

  var p=pos[random(pos.length)];

  $('.fase'+app.current+" .sprite img").css(p.ini);



$('.fase'+app.current+" .sprite img").velocity(
  p.end,{
    duration:3000,
    easing:[250,15],
    complete:function(){
      setTimeout(function(){
        if(app.current<=1){

            monster();


        }
      },2000+Math.floor(Math.random()*1000));
    }
  }


).velocity("reverse");





/*
  $.Velocity.Easings.myCustomEasing = function (p, opts, tweenDelta) {
      return 0.5 - Math.cos( p * Math.PI ) / 2;
  };*/

}
