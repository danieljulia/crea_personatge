var posX = 0;
var posY = 0;


var click = {
    x: 0,
    y: 0
};


// This is better
$(document).mousemove(function(e){

    $('#status').html(e.pageX +', '+ e.pageY);
    posX = e.pageX-$('.main-content').offset().left;
    posY = e.pageY-$('.main-content').offset().top;


    var arw=$('#main').width()/$('#wrap').width();
      var arh=$('#main').height()/$('#wrap').height();
    if(arw<0.9){
      fullscreen=true;
    }else{
      fullscreen=false;
    }

    if(fullscreen){
    //  var arw=$('#main').width()/$('#wrap').width();
      //  var arh=$('#main').height()/$('#wrap').height();

      posX=posX*arw+80; //desfasament
        posY=posY*arh;
        //console.log(arw,arh,posX,posY);
    }
    $('#debug').html(posX+" "+posY);
});


function DragPaint(contenidor){
    this.contenidor=contenidor;
    var that=this;

   this.reset();

   $('body').on('click','.perso img.part',function(){

     var id=$(this).data('id');
     that.toTop(id);

     //that.unRegister(id);
      //$(this).remove();
      //playAudio("SoundError");
   });
}


DragPaint.prototype.reset=function(){
  var como=0;
  if(this.misc!=undefined){
    if( this.misc.como!=0 ){
      como=this.misc.como;
    }
  }

  this.z=1;
  $('.fase3  img.part').remove();
  $('img.mon').attr('src','');
  this.part="ulls";
  this.perso=[];
  this.personatge;
  this.misc={
    como:como,
    pecu_text:'',
    nom:'',
    mon:-1
  };
}

DragPaint.prototype._checkpart=function(part){
  var has_it=false;

  if(this.personatge.data[part].num==0) return true;

  for(var i=0;i<this.perso.length;i++){
    if(this.perso[i].part==part){
      has_it=true;
    }
  }
  return has_it;
}

DragPaint.prototype.validate=function(){
  // te ulls?

  if(config.ignore_validation) return "";


  if(!this._checkpart("ulls")){
    return "ulls";
  }

  if(!this._checkpart("nas")){
    return "nas";
  }

  if(!this._checkpart("boca")){
    return "boca";
  }


  if(this.misc.pecu_text==''){
    return "pecu_text";
  }

  if(!this._checkpart("pecu")){
    return "pecu";
  }

  if(this.misc.mon==-1){
    return "mon";
  }

  return "";





  //te peculiariat text
  //te peculiaritat imatges ?

  //te mon
}


DragPaint.prototype.setPersonatge=function(personatge){
  this.reset();
  var that=this;
  this.personatge=this._getPersonatge(personatge);


  if(this.personatge.data['nas'].num==0){
    $('.personalitat .nas').hide();
  }else{
      $('.personalitat .nas').show();
  }


  this.color=this.personatge.data.colors[0];
  this._updatePerso();
  html="";
  for(var i=0;i<this.personatge.data.colors.length;i++){
    html+="<li data-color='"+i+"' class='pop color_"+this.personatge.data.colors[i]+"'>"+this.personatge.data.colors[i]+"</li>";
  }

  $('.paleta').addClass('color_'+this.personatge.data.colors[0]);
  $('.paleta').removeClass("llimona");
$('.paleta').removeClass("pastanaga");
$('.paleta').removeClass("et");
$('.paleta').removeClass("monstre");
$('.paleta').addClass(personatge);

  $('.esquerra .colores').html(html);

    $('.esquerra .colores li').on('mouseover',function(){
      playAudio("plop");
      });

  $('.esquerra .colores li').on('click',function(){
    var indx=$(this).data('color');
    that.setColor(indx);
    playAudio("SoundMenuSelect");

  });
  //this.init();
}

DragPaint.prototype._updatePerso=function(){
    var pos=this.personatge.data.pos;
  $('.fase3 .perso .perfil').attr('src','images/perfils/'+this.personatge.name+'_'+this.color+'.png');
  $('.fase3 .perso .perfil').css({left:pos[0]+'px',top:pos[1]+'px',transform:'scale('+pos[2]+')'});

}

DragPaint.prototype.setPart=function(part){
  if(part=="pecu") $('.paleta').addClass("pecu");
  else $('.paleta').removeClass("pecu");

  this.part=part;
  this.init();
  if(this.part=="pecu" && this.misc.pecu_text==""){
    app.popuppecu();
  }

  playAudio("SoundMenuSelect");
}

DragPaint.prototype.setColor=function(num){
  this.color=this.personatge.data.colors[num];
  var p=$('.paleta');
  $(p).removeClass();
  $(p).addClass("paleta");
  $(p).addClass('color_'+this.color);
  this._updatePerso();
}

DragPaint.prototype._getPersonatge=function(name){
  for(var i=0;i<personatges.length;i++){

    if(personatges[i].name==name){

      return personatges[i];
    }
  }
}

DragPaint.prototype.unRegister=function(id){
  for(var i=0;i<this.perso.length;i++){
    if(this.perso[i].id==id){
      var src=this.perso[i].img.src;

      this.paletaRestore(src);
      this.perso.splice(i,1);

      return;
    }
  }
}

DragPaint.prototype.changePos=function(id,left,top){

  for(var i=0;i<this.perso.length;i++){
    if(this.perso[i].id==id){
      this.perso[i].img.left=parseInt(left);
      this.perso[i].img.top=parseInt(top);
    }
  }

}

DragPaint.prototype.toTop=function(id){
  var max=1;
  for(var i=0;i<this.perso.length;i++){
    if(this.perso[i].z>max){
      max=this.perso[i].z;
    }
  }

  this.z=max+1;
  for(var i=0;i<this.perso.length;i++){
    if(this.perso[i].id==id){
      this.perso[i].z=this.z;

    }
  }

//  $('img[data-id="'+id+'"]').remove();
  $('img[data-id="'+id+'"]').css('z-index',this.z);

}


DragPaint.prototype.clearCurrentPart=function(){

  for(var i=0;i<this.perso.length;i++){
    if(this.perso[i].part==this.part){
      var id=this.perso[i].id;
      this.unRegister(id);
      //this.perso.splice(i,1);

      this.removeElem(id);
      return;
    }
  }
}

DragPaint.prototype.countParts=function(part){
  var c=0;
  for(var i=0;i<this.perso.length;i++){
    if(this.perso[i].part==part) c++;
  }
  return c;


}

DragPaint.prototype.removeElem=function(id){

  var that=this;
  $('.fase3 .perso .part').each(function(i){

    if($(this).data('id')==id) $(this).remove();
  //  var src=$(this).attr('src');
  //  that.paletaRestore(src);

  });
}

DragPaint.prototype.register=function(id,src,left,top,z){

  //si ja existeix un tros que és d'aquesta part eliminar
  if(this.part!="pecu") this.clearCurrentPart();
  if(this.part=="pecu" && this.countParts("pecu")==5){

    app.error_lang("max_5");
    return false;
  }
  this.perso.push({
    id:id,
    z:z,
    part:this.part,
    img:{src:src,left:left,top:top}
  });

  return true;
}

DragPaint.prototype.getComoText=function(){
  var como=eval("como_"+lang);
  var nom=como[this.misc.como];
  return nom;
}

DragPaint.prototype.paint=function(){

  $('.fase5').html('<h2 class="nom"></h2><div class="perso"><div class="carta"></div></div>');





  $('.fase5 h2').text(this.misc.nom+": "+this.getComoText()+", "+this.misc.pecu_text);

  //posa fons
  if(this.misc.mon==-1) this.misc.mon=0;
  $('.fase5').css('background-image','url(images/fons/'+mons_img[this.misc.mon]+'.png');

  //posa personatge
  $('.fase5 .perso .carta').append('<img  class="perfil" src="images/perfils/'+this.personatge.name+'_'+this.color+'.png">');
      var pos=this.personatge.data.pos;

  $('.fase5 .perso .perfil').css({left:(pos[0]+310)+'px',top:(pos[1]+155)+'px',transform:'scale('+pos[2]+')'});
    //posa detalls
    for(var i=0;i<this.perso.length;i++){
      var p=this.perso[i];
      var img=$('<img />', {
          src: p.img.src,
          class:'part'
        }).appendTo($('.fase5 .perso .carta'));

          $(img).css({'z-index':p.z,left:(p.img.left+9),top:(p.img.top+75)});
    }

}

DragPaint.prototype.existsInPersonatge=function(src){
  for(var i=0;i<this.perso.length;i++){
    if(this.perso[i].img.src==src){
        return true;
    }
  }
  return false;
}

DragPaint.prototype.init=function(){

  var part=this.personatge.data[this.part];
  if(part.pos!=undefined){
    this.pos=part.pos;
  }else{
    this.pos=null;
  }

  $(this.contenidor).html('');
  for(var i=0;i<part.num;i++){
    //sino forma part del personatge
    var img=this.getSrc(this.personatge.name,this.part,i);

    var html="<div class='part pop'><img src='"+img+"'></img></div>";
    $(this.contenidor).append(html);
    if(this.existsInPersonatge(img)){
      $('.paleta .part img[src="'+img+'"]').parent().css('display','none');

    }


  }
  this.initEvents();
}


DragPaint.prototype.getSrc=function(name,part,i){
    var img="images/trossos/"+name+"_"+part+"_"+(i+1)+".png";
    return img;
}


DragPaint.prototype.paletaRemove=function(src){
  $('.paleta .part').each(function(i){
      if( $(this).find('img').attr('src')==src){
        $(this).css('display','none');
      }
  });
}


DragPaint.prototype.paletaRestore=function(src){
  $('.paleta .part').each(function(i){
      if( $(this).find('img').attr('src')==src){
        $(this).css('display','block');
        $(this).css('opacity','1');
      }
  });
  $('.paleta .part img').css('display','block');
}

DragPaint.prototype.initEvents=function(){
  var that=this;
  var w,h;


function setEvents(){
  $( "img.part.erasable" ).on('click',function(){
      playAudio("SoundError");
          var id=$(this).attr('data-id');
    that.unRegister(id);
    $(this).remove();
  });


    $( "img.part.movable" ).draggable({ //quan es mou una peça que ja està posada
      start: function (e, ui) {
          playAudio("SoundPartUp");
           w=ui.helper[0].naturalWidth;
           h=ui.helper[0].naturalHeight;
          var id=$(ui.helper).attr('data-id');
           that.toTop(id);
        },
      drag:function(event,ui){

        w=ui.helper[0].naturalWidth;
        h=ui.helper[0].naturalHeight;

        ui.position.left=posX; //posX- w/ 2 ;
        ui.position.top=posY;//posY - h/ 2 ;


        //if(!fullscreen){
      /*  if(fullscreen){
          posX=0.4*posX; //todo ajustar
          posY=0.4*posY;
        }*/

        if(posX>900 || posX<200){
          console.log("eliminat posX",posX);
          var id=$(ui.helper).attr('data-id');

            $(this).remove();
            playAudio("SoundError");
          that.unRegister(id);
          $(document).trigger("mouseup")
             return;
        }
      //}
    },
    stop:function(event, ui ) {
      var id=$(ui.helper).attr('data-id');
      var left=$(ui.helper).css('left');
      var top=$(ui.helper).css('top');
      that.changePos(id,left,top);

    }

    });
}


  $( ".part img" ).draggable({
    opacity: 0.9,
    helper: "clone" ,
    revert:false,
    start: function (e, ui) {

        playAudio("SoundPartUp");

      //  $(this).css('opacity',0);
        $(this).css('display','none');

          $(ui.helper).css('z-index',that.z);
          $(ui.helper).css('max-height','none');
         w=ui.helper.prevObject[0].naturalWidth;
         h=ui.helper.prevObject[0].naturalHeight;

          $(ui.helper).css({
              width: w,
              height: h
          });

      },
    // cursorAt: {left:parseInt(w/4), top:parseInt(h/4)}, //todo falta centrar respecte imatge!
     //cursorAt: {left:w/2, top:h/2},

    drag:function(event,ui){


        /*
        if(fullscreen){
          posX=0.4*posX; //todo ajustar
          posY=0.4*posY;
        }*/
      //centrar el cursor a l'objecte!!
      ui.position.left=posX- Math.floor( ui.helper.outerWidth()/ 2 );
      ui.position.top=posY - Math.floor( ui.helper.outerHeight()/ 2 );


},

    drop:function(event, ui ) {

      //  $(this).css('opacity',1);

    },
    stop: function( event, ui ) {


    }

  });

  $( "#wrap" ).droppable({
     accept: ".part img",
     classes: {
       "ui-droppable-active": "ui-state-active",
       "ui-droppable-hover": "ui-state-hover"
     },
     drop: function( event, ui ) {
        lp=250;
        tp=0;

        /*
        if(fullscreen){
          posX=0.4*posX; //todo ajustar
          posY=0.4*posY;
        }
*/

        if(that.pos!=null){
          var dx=(posX-that.pos[0]-lp);
          var dy=(posY-that.pos[1]-tp);
          var d=Math.sqrt( (dx*dx)+(dy*dy) );
          $('#debug').text(d);

        }


        if( (that.pos!=null && d<200) || that.pos==null  ){

          var element = $(ui.draggable).clone();
          var id=random(999999999999);

          var left,top;
          if(that.pos==null){
            left=posX;
            top=posY;
          }else{
            left=lp+that.pos[0];
            top=tp+that.pos[1];
          }


          if( that.register(id,$(element).attr('src'),left,top,that.z)){
            that.z++;

            var src=$(element).attr('src');
            that.paletaRemove(src);


            $('.fase3 .perso').append(element);

            if(that.pos==null){

              $(element).css({position:'absolute',left:left,top:top,'z-index':that.z}); //todo afinar
            }else{
              //animacio
              $(element).css({position:'absolute',left:posX,top:posY,'z-index':that.z});

              $(element).velocity({
              left:left,top:top
              },{
                duration:100,
                complete:function(){

                }
              });


              //$(element).css({position:'absolute',left:left,top:top});
            }
            $(element).css('display','block');

            $(element).addClass("part");
            if(that.pos==null) $(element).addClass("movable");
            else $(element).addClass("erasable");
          //  $(element).data('id',id);
            $(element).attr('data-id',id);
            //memoritzar
            //mirar si ja existeix part i substituir si cal

            setEvents();



            playAudio("SoundPartDown");
          }else{
            playAudio("SoundError");
          }
        }else{
          playAudio("SoundError");
        }



     }
   });

}


/*
var paint=new DragPaint('.icones');
paint.setPersonatge("llimona");
*/
