function whenReady() { }

function timer() { }

function woj_main()
	{
	var _THIS = null;

	this.now = 0;
	this.audios = [];
	this.zoomscale = 1;
	this.zoomscaleFX = 0;
	this.zoomscaleFY = 0;

    this.init = function()
		{
		_THIS = this;

		$(window).resize(function() { _THIS.resize();  });
		$(window).scroll(function() { _THIS.scroll();	});

		$(document).ready(function() {

			$(document).on('webkitfullscreenchange mozfullscreenchange fullscreenchange MSFullscreenChange', function() { setTimeout(function () { _THIS.fullScreenChange();},500); });

			$('.scenego').on('click',function() {

				_THIS.change_screen($(this).attr('data-scenego'));
			});

			$('.sceneover').on('click',function() {

				$('#'+$(this).attr('data-sceneover')).fadeIn(400,function() {

					});

			});

			$('.closeparent').on('click',function() {

				$(this).parent().fadeOut(400,function() {

					});

			});


			$('.fullscreen').on('click',function() {

				if(window.innerHeight == screen.height) {

				 	_THIS.deactivateFullScreen();

				} else {

					_THIS.activateFullScreen($('#wrap')[0]);

				}
			});

			whenReady();

			_THIS.timer();
			_THIS.resize();
			});

		$(window).on('load', function () {

			_THIS.resize();
			});
		};


	this.timer = function()
		{
		_THIS.now = new Date().getTime();

		timer();

		setTimeout(function() { _THIS.timer(); },50);
		};


	this.resize = function()
		{
		var ww=$(window).width();
		var wh=$(window).height();

		var dwh=$('.wos-dw').height();

		$('#wos-mid').css('paddingBottom',dwh+'px');
		$('#wos-all').css('marginBottom',-dwh+'px');


		this.scroll();
		};


	this.scroll = function()
		{
		var st=$(window).scrollTop();

		};


	this.change_screen = function(id)
		{
		var f = false;

		$('.scene').each(function() {

			if ($(this).is(":visible")) {

				$(this).fadeOut(400,function() {

					f = true;

					$('#'+id).fadeIn(400,function() {

						});


					});
				}
			});

		if (!f) {

			$('#'+id).fadeIn(400,function() {

				});

			}


		$('.pizarra').each(function() {

			const context = $(this)[0].getContext('2d');

			context.clearRect(0, 0,  $(this)[0].width,  $(this)[0].height);
			});




		screenChanged(id);
		}


	this.preloadImage = function(a) {

	    var i = new Image();
	    i.src=a;

	    i.onload = function() {

	    }
	}



	this.define_audio = function(n,basename,callbackonend,loop)
		{
		var addext='';/*'?'+new Date().getTime();*/

		this.audios[n] = new Howl({
			src: ['sounds/'+basename+'.mp3'+addext],
			autoplay: false,
			loop: loop,
			volume: 1.0,
			onend:callbackonend,
			buffer: false
			});



		};

	this.play_audio = function(n)
		{
		this.audios[n].play();
		};

	this.stop_audios = function()
		{
		for (t=0; t<this.audios.length; t++)
			{
			if (this.audios[t])
				{
				this.stop_audio(t);
				}
			}
		};

	this.stop_audio = function(n)
		{
		this.audios[n].stop();
		};

	this.callbackWhilePlaying = function(n,callback)
		{
		var $howl = this.audios[n];

		this.initHowlOnWhilePlaying($howl);

		this.audios[n].on('whileplaying', function(soundId, data){ callback(soundId, data); });
		};


	this.initHowlOnWhilePlaying = function($howl) {
	  $howl.interval, $howl._onwhileplaying = [];
	  $howl.on('play', function (soundId) {
	    $howl.interval = setInterval(function () {
	      $howl._emit('whileplaying', soundId, [$howl.duration(), $howl.seek()]);
	    }, 100);
	  });
	  $howl.on('stop', function(){
	    clearInterval($howl.interval);
	  });
	  $howl.on('end', function(){
	    clearInterval($howl.interval);
	  });
	}



	this.activateFullScreen = function(elem) {

	

  		if (elem.requestFullscreen) {
		    elem.requestFullscreen();
		  } else if (elem.mozRequestFullScreen) {
		    elem.mozRequestFullScreen();
		  } else if (elem.webkitRequestFullscreen) {
		    elem.webkitRequestFullscreen();
		  } else if (elem.msRequestFullscreen) {
		    elem.msRequestFullscreen();
		} else {

			this.fullScreenChangeDo(1);
		}

	  };

	  this.deactivateFullScreen = function() {



		  if (document.exitFullscreen) {
             document.exitFullscreen();
         } else if (document.webkitExitFullscreen) {
             document.webkitExitFullscreen();
         } else if (document.mozCancelFullScreen) {
             document.mozCancelFullScreen();
         } else if (document.msExitFullscreen) {
             document.msExitFullscreen();
         } else {
			 this.fullScreenChangeDo(0);
		 }

  	  };



	this.fullScreenChange = function() {


		if( window.innerHeight == screen.height) {

			this.fullScreenChangeDo(1);

		} else {

			this.fullScreenChangeDo(0);
		}
	};

	this.fullScreenChangeDo = function(d) {

		var wrap_sx = 1024;
		var wrap_sy = 616;
		var wrap_full = 716;

		if (d == 1) {

			var debugtxt="";

			/*
			var ww=$("#wrap").innerWidth();
			var wh=$("#wrap").height();
			*/

			$("#wrap").css('width','100%');
			$("#wrap").css('height','100%');


			var ww=$(window).width();
			var wh=$(window).height();

			debugtxt+="wrap_sx "+wrap_sx+'\n';
			debugtxt+="wrap_sy "+wrap_sy+'\n';
			debugtxt+="ww "+ww+'\n';
			debugtxt+="wh "+wh+'\n';
			debugtxt+="wos_up_h "+$(".wos-up").height()+'\n';
			debugtxt+="wos_dw_h "+$(".wos-dw").height()+'\n';


			var whr = wh - ($(".wos-up").height() + $(".wos-dw").height());
			console.log(whr);

			scale = (ww) / wrap_sx;
			console.log(scale);

			var wrap_nsx = 0;
			var wrap_nsy = wrap_sy * scale;
			console.log(wrap_nsy);

			if (wrap_nsy > whr) {

				scale = whr / wrap_sy;
			}

			wrap_nsx = wrap_sx * scale;
			wrap_nsy = wrap_sy * scale;

			this.zoomscale = scale;

			var fx = parseInt((ww-wrap_nsx) * 0.5);
			var fy = $(".wos-up").height() + parseInt((whr-wrap_nsy) * 0.5);

			this.zoomscaleFX = fx;
			this.zoomscaleFY = parseInt((whr-wrap_nsy) * 0.5);

			$('.scenescontainer').css('position','fixed');
			$('.scenescontainer').css('margin','0px'); /*margin: 0px 12px;*/

			$('.scenescontainer').css('left',fx+'px');
			$('.scenescontainer').css('top',fy+'px');

			$(".scenescontainer").css('-ms-transform-origin', '0 0');
			$(".scenescontainer").css('transform-origin', '0 0');
			$(".scenescontainer").css('transform-translate', '0 0');

			// Req for IE9
			$(".scenescontainer").css('-ms-transform', 'scale(' + scale + ')');
			$(".scenescontainer").css('transform', 'scale(' + scale + ')');

			$('.fullscreen').find("img").attr('src',$('.fullscreen').find("img").attr('data-img-hi'));

		} else {

			this.zoomscale = 1.0;
			this.zoomscaleFX = 0;
			this.zoomscaleFY = 0;


			$("#wrap").css('width',wrap_sx+'px');
			//$("#wrap").css('height',wrap_sy+'px');
			$("#wrap").css('height',wrap_full+'px');


			$('.scenescontainer').css('position','relative');
			$('.scenescontainer').css('margin','0px 12px');

			$('.scenescontainer').css('left','auto');
			$('.scenescontainer').css('top','auto');

			$(".scenescontainer").css('-ms-transform-origin', '0 0');
			$(".scenescontainer").css('transform-origin', '0 0');
			$(".scenescontainer").css('transform-translate', '0 0');
			// Req for IE9
			$(".scenescontainer").css('-ms-transform', 'scale(1)');
			$(".scenescontainer").css('transform', 'scale(1)');

			$('.fullscreen').find("img").attr('src',$('.fullscreen').find("img").attr('data-img-low'));
		}


	};

	this.init();
	};


var wojMain = new woj_main();
