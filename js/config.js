var config={
  lang:"ca",
  sound:true,
  start:0, //pàgina on comença
  debug:0, //mostra debug
  ignore_validation:false,  //ignora la validació (prous ulls, boca etc)
  canvas:true //converteix a canvas i exporta com imatge al clicar
};
