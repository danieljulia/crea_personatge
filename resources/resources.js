var personatges=[
  {
  "name":"llimona",
  data:{
      "pos":[76,52,0.9], //1
      "colors":['g','v'],
    "ulls":{num:7,pos:[290,235]},
    "boca":{num:7,pos:[295,300]},

    "nas":{num:0},
    "pecu":{num:17},

  }},

  {"name":"pastanaga",
  data:{
      "pos":[116,-30,0.9], //1
      "colors":['o','t'],
    "ulls":{num:6,pos:[265,210]},
    "boca":{num:6,pos:[262,255]},
    "nas":{num:0},
    "pecu":{num:17},

  }
},

  {"name":"et",
  data:{
      "pos":[150,-5,0.9],
      "colors":['r','v','b','g'],
    "ulls":{num:10,pos:[285,160]},
    "boca":{num:13,pos:[292,230]},
    "nas":{num:7,pos:[287,200]},
    "pecu":{num:17},

  }
},
{"name":"monstre",
data:{
  "pos":[60,6,0.9],
  "colors":['v','b2','g2','r2'],
  "ulls":{num:8,pos:[180+78,150+28]},
  "boca":{num:7,pos:[235+28,277+30]},
  "nas":{num:6,pos:[240+26,230+25]},
  "pecu":{num:17},

}
}

]



//todo
var como_ca=['Tímid','Entusiasta','Babau','Poruc','Murri','Somiador',
'Rondinaire','Enginyós','Optimista','Covard','Bergant','Tocat del bolet'];
var como_es=['Tímido','Entusiasta','Bobo','Tristón','Pillo','Soñador',
'Gruñon','Ingenioso','Astuto','Cobarde','Bellaco','Loco de remate'];

var pecu_ca=['Passa desapercebut','Practica esports','Li encanta la platja','Viu a la cuina','Viatja a l\'espai','Va a la moda',
'Viu aventures'];
var pecu_es=['Pasa desapercebido','Practica deportes','Le encanta la playa','Vive en la cocina','Viaja al espacio','Va a la moda',
'Vive aventuras'];

var mons_ca=['Al desert','A l\'espai','Entre volcans','Al mar'];
var mons_es=['En el desierto','En el espacio','Entre volcanes','En el mar'];
var mons_img=['fons_desert','fons_planetes','fons_volcans','fons_mar'];

var labels={
  titol_es:"Crea tu personaje",
  crea_es:"Crea tu <br>personaje",
  info_es:"<p>En las peliculas de animación encontramos personajes que nos sorprenden y nos emocionan.</p><p>Te invitamos a construir tu personaje.</p><p>¿Qué personalidad y que aspecto puede tener?</p><p>¿Qué le gustará? ¿Como quieres que se llame?</p><p>¡Muestra tu imaginación y sorpréndenos con tu creación!</p>",
  ulls_es:"Ojos",
  nas_es:"Nariz",
  boca_es:"Boca",
  pecu_es:"Peculiaridad",
  llest_es:"¡Listo!",
  perso_es:"Personalidad:",
  mons_es:"Mundos",
  nom_es:"¡Ahora solo le falta poner un nombre!",
  escull_es:"Escoge tu personaje",
  max_5_ca:"No pots col·locar més de 5 complements!",
  com_ca:"Com és?",
  com_es:"¿Cómo es?",
  vols_ca:"Vols aquest?",
    vols_es:"¿Quieres este?",
  credits_es:"Créditos",
  max_5_es:"¡No puedes colocar mas de 5 complementos!",
  avis_nas_ca:'Li falta el nas al teu personatge!',
  avis_nas_es:'¡Le falta la nariz a tu personaje!',
  avis_ulls_ca:'Li falten el ulls al teu personatge!',
  avis_ulls_es:'¡Le faltan los ojos a tu personaje!',
  avis_boca_ca:'Li falta la boca al teu personatge!',
  avis_boca_es:'¡Le falta la boca a tu personaje!',
  avis_pecu_ca:'Has d\'afegir-li alguna peculiaritat!',
  avis_pecu_es:'¡Tienes que añadirle alguna peculiaridad!',
avis_pecu_text_ca:'Has d\'afegir-li alguna peculiaritat!',
avis_pecu_text_es:'¡Tienes que añadirle alguna peculiaridad!',
on_ca:'On viu el teu personatge?',
on_es:'¿Donde vive tu personaje?',
teu_ca:"El teu personatge...",
teu_es:"Tu personaje...",
credits_es:"CRÉDITOS",
credits_msg_ca:"<h3 class='titol'>CRÈDITS</h2><p class='info'>Aquest recurs és una idea de Digital Films per l'Obra Social \"La Caixa \". <br><br>Concepció: Alexis Borràs<br>Assessorament pedagògic: Blanca Almendáriz<br><br>Storyboard, disseny i desenvolupament: Ya! yaki<br>.disseny: Miriam Sugranyes<br>.desenvolupament: Victor Chimeno<br><br>Il·lustracions: Miguel Gallardo </p> ",
credits_msg_es:"<h3 class='titol'>CRÉDITOS</h2><p class='info'>Este recurso es una idea de Digital Films para la Obra Social \"La Caixa \". <br><br>Concepción: Alexis Borràs<br>Asesoramiento pedagógico: Blanca Almendáriz<br><br>Storyboard, diseño y desarrollo: Ya! yaki<br>.diseño: Miriam Sugranyes<br>.desarrollo: Victor Chimeno<br><br>Ilustraciones: Miguel Gallardo </p> ",
mon_ca:'Has de triar on viu el teu personatge!',
  mon_es:"¡Tienes que escoger donde vive tu personaje!",

}
